# connect-ssi

A node Connect middleware to support SSI-like functionality

## Usage

`connect-ssi.js` exports a single function which accepts a host url parameter and returns a connect middleware.

When the middleware encounters the following in an html file:

```html
<!-- #include virtual="/test.ssi" -->
```

it will be replaced with the contents of `<host url>/test.ssi`. All relative paths in the included file will updated to include the host url. Note only virtual includes are supported.

### Installation

Add

```javascript
"connect-ssi": "git+https://bitbucket.org/nsidc/connect-ssi.git"
```

to the devDependencies section in your `package.json` file and run `npm install`.

### Connect configuration

In your `Gruntfile.js` you must configure Connect to use middleware, for example:

```javascript
connect: {
    server: {
        options: {
          hostname: '',  // Setting host name to empty string allows port forwarding to work on VMs
          port: 3000,
          base: 'src',
          keepalive: true,
          middleware: function (connect, options) {
            var virtualSsi = require('connect-ssi');
            return [
              virtualSsi('http://testhost.com'),
              connect.static(options.base)
            ];
          }
        }
    }
}
```

See [https://github.com/gruntjs/grunt-contrib-connect](https://github.com/gruntjs/grunt-contrib-connect) for more information about setting up middleware.