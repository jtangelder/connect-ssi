module.exports = function virtualSSI(host) {

  return function(req, res, next) {
    var WritableStream = require("stream-buffers").WritableStreamBuffer,
        httpRequest = require("request"),
        _ = require("underscore"),
        buffer;

    if(req.url != "/" && !req.url.match(/\.html/))
      return next();

    buffer = new WritableStream();
    res.write = function(chunk) {
      buffer.write(chunk);
      return true;
    };

    var oldEnd = res.end;
    res.end = function(data) {
      var self = this, body, includes, remaining;
      if(data) {
        buffer.write(data);
      }

      if (!buffer.size()) {
        return oldEnd.call(self, buffer.getContents());
      }

      body = buffer.getContentsAsString();
      includes = body.match(/<!--\s*#include virtual=".*"\s*-->/g);
      if (!includes) {
        return oldEnd.call(self, body);
      }


      remaining = includes.length;

      _.each(includes, function(include) {
        var url = host + include.match(/<!--\s*#include virtual="(.*)"\s*-->/)[1];

        httpRequest(url, function(error, response, data) {
          if(error || response.statusCode !== 200) {
            console.log("ERROR including file " + url + ": " + error);
          }
          else {
            body =  body.replace(include, data.replace(/href="\//g, "href=\"" + host + "/").replace(/src="\//g, "src=\"" + host + "/"));
          }

          if (!--remaining) {
            self.setHeader("content-length", body.length);
            oldEnd.call(self, body);
          }
        });
      });
    }

    next();
  }
}
